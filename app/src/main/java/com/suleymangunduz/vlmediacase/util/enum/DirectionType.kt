package com.suleymangunduz.vlmediacase.util.enum

enum class DirectionType {
    LEFT, RIGHT, NONE
}