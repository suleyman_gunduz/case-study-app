package com.suleymangunduz.vlmediacase.util

import android.widget.ImageView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.android.material.textview.MaterialTextView
import com.suleymangunduz.vlmediacase.R

@BindingAdapter("android:src")
fun ImageView.loadImage(url: String?) {
    if (url.isNullOrEmpty()) return
    Glide.with(this).load(url).into(this)
}

@BindingAdapter("status")
fun MaterialTextView.status(status: String) {
    text = status.capitalize()
    with(status) {
        when {
            contains("Alive") -> setDrawableLeft(R.color.status_alive_color)
            contains("Unknown") -> setDrawableLeft(R.color.status_unknown_color)
            startsWith("Dead") -> setDrawableLeft(R.color.status_dead_color)
            else -> setDrawableLeft(R.color.status_unknown_color)
        }
    }
}

private fun MaterialTextView.setDrawableLeft(@ColorRes res: Int) {
    if (compoundDrawables[0] == null) return
    compoundDrawables[0].setTint(ContextCompat.getColor(context, res))
}