package com.suleymangunduz.vlmediacase.util

class Constants {
    object ServerConstants {
        const val BASE_URL = "https://rickandmortyapi.com/api/"
        const val PAGE_NUMBER_SPLIT_KEY = "page="
        const val TIMEOUT_IN_SEC = 30L
    }
}