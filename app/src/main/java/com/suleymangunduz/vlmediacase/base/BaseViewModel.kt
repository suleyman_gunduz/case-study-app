package com.suleymangunduz.vlmediacase.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.suleymangunduz.vlmediacase.model.Info
import com.suleymangunduz.vlmediacase.util.Constants
import com.suleymangunduz.vlmediacase.util.SingleLiveEvent
import kotlinx.coroutines.Job
import timber.log.Timber
import java.net.SocketException
import java.net.UnknownHostException

abstract class BaseViewModel : ViewModel() {
    var errorMessage = SingleLiveEvent<String>()
    val progressBarVisibility: MutableLiveData<Int> = MutableLiveData()
    var pageNumber: Int? = 1
    var job: Job? = null

    fun getNextPage(info: Info): Int? {
        return info.next?.split(Constants.ServerConstants.PAGE_NUMBER_SPLIT_KEY)?.last()?.toInt()
    }

    fun onError(message: String?) {
        errorMessage.value = message!!
        Timber.e(message)
    }

    fun onException(ex: Exception) {
        errorMessage.postValue(getErrorMessage(ex))
        Timber.e(ex)
    }

    private fun getErrorMessage(ex: Exception): String {
        return when (ex) {
            is SocketException -> "Bad Internet Connection"
            is UnknownHostException -> "No internet Connection"
            else -> "Unrecognized Error"
        }
    }
}