package com.suleymangunduz.vlmediacase.repository

import com.suleymangunduz.vlmediacase.model.Character
import com.suleymangunduz.vlmediacase.model.PageResponse
import retrofit2.Response

interface CharacterRepository {
    suspend fun getAllCharacters(page: Int): Response<PageResponse<Character>>
}