package com.suleymangunduz.vlmediacase.repository

import com.suleymangunduz.vlmediacase.model.Character
import com.suleymangunduz.vlmediacase.model.PageResponse
import com.suleymangunduz.vlmediacase.service.CharacterApi
import retrofit2.Response
import javax.inject.Inject

class CharacterRepositoryImpl @Inject constructor(
    private val api: CharacterApi,
) : CharacterRepository {

    override suspend fun getAllCharacters(page: Int): Response<PageResponse<Character>> {
        return api.getAllCharacters(page)
    }
}