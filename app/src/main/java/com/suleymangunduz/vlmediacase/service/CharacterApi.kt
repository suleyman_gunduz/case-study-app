package com.suleymangunduz.vlmediacase.service

import com.suleymangunduz.vlmediacase.model.Character
import com.suleymangunduz.vlmediacase.model.PageResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CharacterApi {
    @GET("character/")
    suspend fun getAllCharacters(@Query("page") page: Int): Response<PageResponse<Character>>
}