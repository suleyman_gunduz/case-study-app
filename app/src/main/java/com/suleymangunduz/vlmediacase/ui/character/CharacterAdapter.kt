package com.suleymangunduz.vlmediacase.ui.character

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.suleymangunduz.vlmediacase.databinding.CharacterItemBinding
import com.suleymangunduz.vlmediacase.model.Character
import javax.inject.Inject


class CharacterAdapter @Inject constructor() :
    RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {

    private var characters: List<Character>? = null

    fun setCharacterList(characters: List<Character>?) {
        this.characters = characters
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CharacterViewHolder(
            CharacterItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    override fun getItemCount(): Int {
        return characters!!.size
    }

    fun getItem(position: Int): Character {
        return characters!![position]
    }

    inner class CharacterViewHolder(private val binding: CharacterItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Character) = with(binding) {
            ViewCompat.setTransitionName(binding.imageLogo, "imageLogo_${item.id}")
            ViewCompat.setTransitionName(binding.tvName, "name_${item.id}")
            ViewCompat.setTransitionName(binding.tvStatus, "status_${item.id}")
            ViewCompat.setTransitionName(binding.tvLocation, "location_${item.id}")
            character = item
        }
    }
}