package com.suleymangunduz.vlmediacase.ui.character


import android.view.View
import androidx.lifecycle.MutableLiveData
import com.suleymangunduz.vlmediacase.base.BaseViewModel
import com.suleymangunduz.vlmediacase.model.Character
import com.suleymangunduz.vlmediacase.repository.CharacterRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class CharacterListViewModel @Inject constructor(private val characterRepository: CharacterRepository) :
    BaseViewModel() {
    val charactersLiveData = MutableLiveData<List<Character>>()
    var needToNotifyAdapter = true

    init {
        getAllCharacters()
    }

    fun getAllCharacters() {
        pageNumber?.let {
            progressBarVisibility.value = View.VISIBLE

            job = CoroutineScope(Dispatchers.IO).launch {
                try {
                    val response = characterRepository.getAllCharacters(pageNumber!!)
                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            response.body()?.results?.let { data ->
                                needToNotifyAdapter = true
                                postValue(data)
                            } ?: onError("No list data")

                            pageNumber = response.body()?.info?.let { it1 -> getNextPage(it1) }
                        } else {
                            onError(response.message())
                        }

                    }
                } catch (ex: Exception) {
                    onException(ex)
                } finally {
                    progressBarVisibility.postValue(View.GONE)
                }
            }
        }
    }

    private fun postValue(characters: List<Character>) {
        charactersLiveData.value?.let { data ->
            val reversed = characters.reversed() as ArrayList<Character>
            val oldData = data as ArrayList<Character>

            reversed.addAll(oldData)

            charactersLiveData.postValue(reversed as List<Character>)
        } ?: charactersLiveData.postValue(characters.reversed())
    }

    fun deleteOnSwipe() {
        val updated = charactersLiveData.value as MutableList<Character>
        updated.removeAt(updated.size - 1)
        charactersLiveData.postValue(updated)
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}