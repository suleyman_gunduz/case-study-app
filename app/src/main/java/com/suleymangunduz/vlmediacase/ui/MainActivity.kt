package com.suleymangunduz.vlmediacase.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.suleymangunduz.vlmediacase.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}