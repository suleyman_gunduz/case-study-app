package com.suleymangunduz.vlmediacase.ui.character

import android.animation.ObjectAnimator
import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.os.Handler
import android.os.SystemClock
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.suleymangunduz.vlmediacase.R
import com.suleymangunduz.vlmediacase.base.BaseFragment
import com.suleymangunduz.vlmediacase.databinding.FragmentCharacterBinding
import com.suleymangunduz.vlmediacase.ui.character.custom_decoration.OverlappingItemDecoration
import com.suleymangunduz.vlmediacase.util.enum.DirectionType
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class CharacterListFragment : BaseFragment<FragmentCharacterBinding, CharacterListViewModel>() {
    private val characterListViewModel: CharacterListViewModel by viewModels()

    val Int.px: Int get() = (this * Resources.getSystem().displayMetrics.density).toInt()
    private var swipeDirection: DirectionType = DirectionType.NONE
    fun Context.toast(message: CharSequence) =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

    @Inject
    lateinit var characterAdapter: CharacterAdapter

    override val layoutId: Int
        get() = R.layout.fragment_character

    override fun getVM(): CharacterListViewModel = characterListViewModel

    override fun bindVM(binding: FragmentCharacterBinding, vm: CharacterListViewModel) =
        with(binding) {
            setupRecyclerView()
            setupButtons()
            setupObservers()
        }

    private fun setupButtons() {
        binding.run {
            buttonLike.clickWithDebounce { swipeRow(DirectionType.RIGHT) }
            buttonDislike.clickWithDebounce { swipeRow(DirectionType.LEFT) }
            buttonRetry.setOnClickListener { retry() }
        }
    }

    private fun swipeRow(directionType: DirectionType) {
        binding.run {
            val view =
                rvCharacters.findViewHolderForAdapterPosition(characterAdapter.itemCount - 1)?.itemView

            val px =
                if (directionType == DirectionType.RIGHT) 1000.px.toFloat() else -1000.px.toFloat()

            ObjectAnimator.ofFloat(view, "translationX", px).apply {
                duration = 800
                start()
            }

            characterListViewModel.run {
                deleteOnSwipe()
            }

            showToast(directionType)
        }
    }

    private fun showToast(directionType: DirectionType) {
        val message = if (directionType == DirectionType.RIGHT) "Liked!!" else "Unliked!!"
        context?.toast(message)
    }

    private fun retry() {
        binding.run {

            listLayout.visibility = View.VISIBLE
            errorLayout.visibility = View.GONE

            with(viewModel) {
                getAllCharacters()
            }
        }
    }

    private fun setupRecyclerView() {
        binding.run {

            rvCharacters.apply {
                postponeEnterTransition()
                viewTreeObserver.addOnPreDrawListener {
                    startPostponedEnterTransition()
                    true
                }
            }

            rvCharacters.setHasFixedSize(true)
            rvCharacters.addItemDecoration(OverlappingItemDecoration())

            var linearLayoutManager = LinearLayoutManager(context)
            linearLayoutManager.reverseLayout = true
            linearLayoutManager.stackFromEnd = true
            rvCharacters.layoutManager = linearLayoutManager

            setItemTouchCallBack()


        }
    }

    private fun setItemTouchCallBack() {
        binding.run {

            val itemTouchHelperCallback =
                object :
                    ItemTouchHelper.SimpleCallback(
                        0,
                        ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
                    ) {
                    override fun onMove(
                        recyclerView: RecyclerView,
                        viewHolder: RecyclerView.ViewHolder,
                        target: RecyclerView.ViewHolder
                    ): Boolean {
                        return false
                    }

                    override fun getSwipeDirs(
                        recyclerView: RecyclerView,
                        viewHolder: RecyclerView.ViewHolder
                    ): Int {
                        var position = viewHolder.bindingAdapterPosition
                        if (position != characterListViewModel.charactersLiveData.value!!.size - 1) {
                            return 0
                        }
                        return super.getSwipeDirs(recyclerView, viewHolder)
                    }

                    override fun onChildDraw(
                        c: Canvas,
                        recyclerView: RecyclerView,
                        viewHolder: RecyclerView.ViewHolder,
                        dX: Float,
                        dY: Float,
                        actionState: Int,
                        isCurrentlyActive: Boolean
                    ) {
                        super.onChildDraw(
                            c,
                            recyclerView,
                            viewHolder,
                            dX,
                            dY,
                            actionState,
                            isCurrentlyActive
                        )

                        swipeDirection = if (dX > 0) DirectionType.RIGHT else DirectionType.LEFT
                    }

                    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                        showToast(swipeDirection)
                        with(viewModel) {
                            deleteOnSwipe()
                        }
                    }
                }
            val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
            itemTouchHelper.attachToRecyclerView(rvCharacters)
        }
    }

    private fun setupObservers() {
        binding.run {
            with(viewModel) {
                launchOnLifecycleScope {


                    charactersLiveData.observe(viewLifecycleOwner, Observer {

                        if (needToNotifyAdapter) {
                            characterAdapter.setCharacterList(it)
                            rvCharacters.adapter = characterAdapter
                            characterAdapter.notifyDataSetChanged()
                            needToNotifyAdapter = false
                        }

                        if (it.isNotEmpty() && it.size <= 3) {
                            getAllCharacters()
                        }

                        if (it.isNotEmpty() && buttonLayout.visibility == View.GONE) {
                            buttonLayout.visibility = View.VISIBLE
                        }

                    })

                    progressBarVisibility.observe(
                        viewLifecycleOwner,
                        Observer { progressBarVisibility ->
                            progressBar.visibility = progressBarVisibility

                            if (progressBarVisibility == View.GONE && !needToNotifyAdapter) {
                                buttonLike.isEnabled = true
                                buttonDislike.isEnabled = true
                            } else if (progressBarVisibility == View.GONE && needToNotifyAdapter) {
                                Handler().postDelayed({
                                    buttonLike.isEnabled = true
                                    buttonDislike.isEnabled = true
                                }, 900)


                            } else {
                                buttonLike.isEnabled =
                                    progressBarVisibility == View.GONE
                                buttonDislike.isEnabled =
                                    progressBarVisibility == View.GONE
                            }

                        })

                    errorMessage.observe(viewLifecycleOwner,
                        Observer { message ->

                            listLayout.visibility = View.GONE
                            errorLayout.visibility = View.VISIBLE

                            tvError.text = message
                        })

                }
            }
        }
    }

    fun View.clickWithDebounce(debounceTime: Long = 500L, action: () -> Unit) {
        this.setOnClickListener(object : View.OnClickListener {
            private var lastClickTime: Long = 0

            override fun onClick(v: View) {
                if (SystemClock.elapsedRealtime() - lastClickTime < debounceTime) return
                else action()

                lastClickTime = SystemClock.elapsedRealtime()
            }
        })
    }
}