package com.suleymangunduz.vlmediacase.ui.character.custom_decoration

import android.content.res.Resources
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class OverlappingItemDecoration : RecyclerView.ItemDecoration() {
    val Int.px: Int get() = (this * Resources.getSystem().displayMetrics.density).toInt()

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.set(0, 0, 0, (-305).px)
    }
}