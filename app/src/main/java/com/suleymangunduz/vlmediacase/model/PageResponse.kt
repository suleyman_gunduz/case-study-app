package com.suleymangunduz.vlmediacase.model

data class PageResponse<T>(
    val info: Info,
    val results: List<T> = listOf()
)