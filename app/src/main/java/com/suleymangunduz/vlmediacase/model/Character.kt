package com.suleymangunduz.vlmediacase.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Character(
    val id: Int,
    val name: String,
    val status: String,
    val image: String,
    val location: Location
) : Parcelable