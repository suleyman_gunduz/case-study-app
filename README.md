# case-study-app
- The project was developed with MVVM Kotlin.
- Hilt, Retrofit, Gson, Glide, Timber, LiveData, ViewBinding, DataBinding etc. libraries were used
- No swipe card library was used. Recyclerview was used for the swipe card feature.
- Pagination was used for listing operations.
-  When the last 3 or less cards are left in the list, a new pagination request is made.
- When an error is encountered, a refresh button and an error message are displayed so that the user can renew the request.
I hope you will like it.

